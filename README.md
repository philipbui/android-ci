# Android CI

[![Build Status](https://travis-ci.org/philip-bui/android-ci.svg?branch=master)](https://travis-ci.org/philip-bui/android-ci)
[![Docker Pulls](https://img.shields.io/docker/pulls/philipbui/android-ci.svg)](https://hub.docker.com/r/philipbui/android-ci)
[![Docker Build Status](https://img.shields.io/docker/build/philipbui/android-ci.svg)](https://hub.docker.com/r/philipbui/android-ci/builds)
[![Docker Automated build](https://img.shields.io/docker/automated/philipbui/android-ci.svg)](https://hub.docker.com/r/philipbui/android-ci/~/dockerfile)

## Setup

Copy `wait-for-emulator.sh` & `stop-emulators.sh` to the Project directory.

Use `philipbui/android-ci` as your Docker image. It contains a AVD `test` for Android 27.

### Build

```
script:
  - ./gradlew clean assembleRelease

artifacts:
  - app/build/outputs
```

### Unit Tests

```
script:
  - ./gradlew test
  
artifacts:
  - app/build/reports/tests
```

### Instrumentation Tests

```
script:
  - emulator -avd test -no-audio -no-window
  - ./wait-for-emulator.sh
  - adb shell settings put global window_animation_scale 0
  - adb shell settings put global transition_animation_scale 0
  - adb shell settings put global animator_duration_scale 0
  - adb shell input keyevent 82
  - ./gradlew cAT
  - ./stop-emulators.sh

artifacts:
  - app/build/reports/androidTest/connected/
```

## License

```
MIT License

Copyright (c) 2018 Philip Bui

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

